## ShortURL service
 
###  A complete shortlink service for tracking popularity of certain services 

### Author C.W. Epema, 2014

### License: GPL3

### Requires

PHP, MariaDb (MySql), curl, mysqli_extension enabled, Nginx or Lighttpd.
(Not tested on Apache)

### Installation

Copy or clone all files directly to a domain you serve from Github: git@github.com:kzpm/shortlink.git


The file index.php should reside in the root of your domain:


Example:
	/srv/http/mydomain.com
		-index.php	(file)
		-/inc/		(directory)
		-/src/		(directory)
		-/error/	(directory)
		-table.sql	(file)

Edit cst.inc to reflect your Companyname and MySql username, password  and tablename



### What will be stored?

`su_date` char(20) DEFAULT NULL,
  `su_nr` int(9) DEFAULT NULL,
  `su_longurl` text,
  `su_desc` char(200) DEFAULT NULL,
  `su_shorturl` char(50) DEFAULT NULL,
  `su_accessed` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

When someone creates a shortlink, the date, the original long url, the description (if any) the short url (4 characters) and the number of times the link was accessed.

The number of accesses is extremely convenient for organisations, who like to keep track of the popularity of a service!

### Webserver tweaks

Nginx:

```
server {
	listen 80;
	listen [::]:80;

	root /srv/http/yourdomain.com;

	# Add index.php to the list if you are using PHP
	index index.php index.html index.htm;

	server_name yourdomain.com www.yourdomain.com *.yourdomain.com;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ @rewrite;

	}

	location @rewrite {
		
		# Without this rule forwarding url's will not work properly!	
		rewrite ^/(.*)$ /index.php?fwd=$1 last;

	}
	error_page 404 /404.html;

        error_page 500 502 503 504 /50x.html;
        
	location = /50x.html {
                root /usr/share/nginx/html;
        }

}
```

