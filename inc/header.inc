<?php

	require_once $_SERVER["DOCUMENT_ROOT"].'/cst.inc';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>URL Shortener,'.COMPANY.'</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Shorten long url`s">
  <META NAME="ROBOTS" CONTENT="INDEX,FOLLOW">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/png" href="https://i.imgur.com/nAgrYy0.png"</>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css>';
  <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js>';
  <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js>';


<link href='https://fonts.googleapis.com/css?family=Lobster|Averia+Libre|Oxygen|Imprima|Londrina Sketch|Emilys Candy|Vollkorn|Metrophobic|Questrial|Iceland|Ubuntu Mono|Prata|Swanky and Moo Moo|Badaboom|Trocchi' rel='stylesheet' type='text/css'>

<style>
       body {

	   font-family:'Vollkorn';
           font-size:2em;
           line-height:20px;
	   min-height:100%;
	   height:100%;

	  /* background-color:#444;*/

       }


}


</style>
</head>
<body>
<nav class="navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	<span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">'.COMPANY.', Shortlink service</a>
    </div>
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="https://github.com/kzpm/shortlink" target="_blank">Source code</a></li>
    </ul>
  </div>
</nav>

