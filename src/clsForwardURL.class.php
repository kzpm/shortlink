<?php

//error handling
error_reporting( E_ALL | E_STRICT );
ini_set( 'display_errors', 0 );
ini_set( 'log_errors', 1 );
ini_set("error_log", "error/error.log");


class clsForwardURL{


	public function fForwardURL( $query ){


		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){

			$sql = sprintf( "select `su_longurl` from `tblURL` where `su_shorturl` ='%s'", mysqli_real_escape_string( $myConnect::$cn,trim( $query ) ) );
			$result = mysqli_query( $myConnect::$cn, $sql );
			for( $i = 0; $url[$i] = mysqli_fetch_array( $result ); $i++ ) ;
          		array_pop( $url );
			$this->fUpdateAccess( $query );
	                return $url;

		}//end if

	}//end f

	public function fUpdateAccess( $query ){


		require_once 'clsDbConnect.class.php';
                $myConnect = new clsDbConnect();
                if ( $myConnect->fConnect() ){

                        $sql = sprintf( "update `tblURL` set `su_accessed` = `su_accessed`+1 where `su_shorturl` ='%s'", mysqli_real_escape_string( $myConnect::$cn,trim( $query ) ) );
                        $result = mysqli_query( $myConnect::$cn, $sql );
			if ( mysqli_affected_rows( $myConnect::$cn ) ){

				return TRUE;

			}//end fi


                }//end if

	}//end f

}//end class

?>
