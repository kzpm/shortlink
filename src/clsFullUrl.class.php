<?php


class clsFullUrl{


	public function fFullCUrl(){

    		$s = &$_SERVER;
		$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
   		$sp = strtolower($s['SERVER_PROTOCOL']);
    		$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    		$port = $s['SERVER_PORT'];
    		$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
   		$host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    		$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    		$uri = $protocol . '://' . $host . $s['REQUEST_URI'];
    		$segments = explode('?', $uri, 2);
    		$url = $segments[0];
    		return $url;

	}//end f


	public function fFullUrl(){

		$pageURL = 'http';

		if ( $_SERVER["HTTPS"] == "on" ) { $pageURL .= "s"; }

		$pageURL .= "://";

		if ( $_SERVER["SERVER_PORT"] != "80" ) {

			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];	

		} else {

			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

		}

		return $pageURL;
		


	}//end f

}//end class

?>
