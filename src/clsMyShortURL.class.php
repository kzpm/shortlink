<?php

class clsMyShortURL{

	protected $URL;
	protected $resultURL;
	protected $arr= 'abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_';

	public function fResultURL( $URL, $desc ){


		//Sanitize URL
		$URL = filter_var( $URL, FILTER_SANITIZE_URL );
		$URL = $this->fCheckHttpPrefix( $URL );
 
		if ( !empty( $URL ) ){

			$this->resultURL = $this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))];

			if ( !$this->fCheckIfExists( $this->resultURL ) ) {
		

				if ( $this->fStoreURL( $this->resultURL, $URL, $desc )){


					return "http://winlin.nl/".$this->resultURL;

				}//end if

		
			}//end if

		}//end if


	}//end f

	protected function fStoreURL( $resultURL, $URL, $desc ){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		
		
		if ( $myConnect->fConnect() ){

			$sql = sprintf("INSERT INTO `tblURL` values( '%s',%d,'%s','%s','%s',%d)", mysqli_real_escape_string( $myConnect::$cn, date('YmdHis') ),mysqli_real_escape_string( $myConnect::$cn, 0),mysqli_real_escape_string( $myConnect::$cn, $URL ),mysqli_real_escape_string( $myConnect::$cn, $desc ),mysqli_real_escape_string( $myConnect::$cn, $this->resultURL),mysqli_real_escape_string( $myConnect::$cn, 0 ));
			$result = mysqli_query( $myConnect::$cn, $sql );

			if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

				return TRUE;

			}//end  if

		}//end if


	}//end f


	protected function fCheckIfExists( $URL ){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();



		if ( $myConnect->fConnect() ){
        
                                $sql = sprintf("select `su_shorturl` from `tblURL` where `su_shorturl` = '%s'", mysqli_real_escape_string( $myConnect::$cn, $this->resultURL ) );
                                $result = mysqli_query( $sql, $myConnect::$cn );

                                if ( mysqli_num_rows( $result ) > 0 ){


					$this->resultURL = "http://winlin.nl/".$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))];
					return TRUE;

                                }else{

					return FALSE;

				}//end  if

                        }//end if


	}//end f

	protected function fCheckHttpPrefix( $URL ){
		

		if ( strPos( $URL, "http") === FALSE ){
	
			if ( strPos( $URL, "https") === FALSE ){

				if ( $this->fCheckIfUrlExists( "https://".$URL ) ){

					if ( $this->fCheckIfUrlExists( "http://".$URL) ){

						return "http://".$URL;

					}//end if

					return "https://".$URL;


				}//end if

			}//end if


		}//end if	 		


		return $URL;

	}//end f


	protected function fCheckIfUrlExists( $URL ){

		$file_headers = @get_headers( $URL );
		
		//print_r( $file_headers );

		if( $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.1 301 Moved Permanently') {

		    return FALSE;

		}else {

    		    return TRUE;
		}

	}//end f


}//end class


?>
