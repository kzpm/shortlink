<?php
class clsDbConnect{
        static public $db;
        static public $cn;

        public function __construct(){
                self::fConnect();
        }//end f

        /*fConnect is de database connector. resp. host, username, ww, database*/
        public static function fConnect(){

		require_once $_SERVER["DOCUMENT_ROOT"].'/cst.inc';

                if(self::$cn = mysqli_connect('localhost',DBUSER,DBPASS,DBNAME)){

                        if(self::$db = mysqli_select_db(self::$cn, DBNAME)){

                                return TRUE;

                        }else{

                                return FALSE;

                        }//end if

                }else{

                        return FALSE;

                }//end if

        }//end f


}//end class

