<?php

//error handling
error_reporting( E_ALL | E_STRICT );
ini_set( 'display_errors', 0 );
ini_set( 'log_errors', 1 );
ini_set("error_log", "error/error.log");

// Load clsFullUrl to parse URL argument passed by user
require_once $_SERVER["DOCUMENT_ROOT"].'/src/clsFullUrl.class.php';
$myUrl = new clsFullUrl();
$myUrl->fFullUrl();

// Load clsValidUrl class to verify integrity and validity of URL
require_once $_SERVER["DOCUMENT_ROOT"].'/src/clsValidUrl.class.php';
$myValidUrl = new clsValidUrl();

// Test if URL is not empty. Prevent linkage to linkage of index.* or files to root filesystem files
if ( !empty( $myUrl ) && $_SERVER['REQUEST_URI'] !== '/index.php' && $_SERVER['REQUEST_URI'] !== '/index.html' && $_SERVER['REQUEST_URI'] !== '/' ){

	$query = htmlspecialchars( str_replace("fwd=","", $_SERVER['QUERY_STRING']) );

	// Load forward string and store short url, logurl and description into table
	require_once $_SERVER["DOCUMENT_ROOT"].'/src/clsForwardURL.class.php';
	$myForward = new clsForwardURL();
	$url = $myForward->fForwardURL( $query );
	$url = ( $url[0]['su_longurl'] );
	echo header( 'location:'.$url );


}//end if

//  Load css and bootstrap
require_once $_SERVER["DOCUMENT_ROOT"].'/inc/header.inc';


// Form 
echo '
<div class ="container">
	<br><br>
	Shorten URL:
	<form action = "#" method ="POST"><br>

	<input type="text" class="form-control" name= "txtLongURL">
	<br>
	<br>
	Link description:
	<br>
	<br>
	<input type="text" class="form-control" name= "txtDescription" >
	<input class="btn btn-info" type="submit" name= "btnSubmit" value="OK"></button>
	</form>
</div>
';

//Implementation and processing of form
if ( isset( $_POST["btnSubmit"] ) ){


	$URL = strtolower( $_POST["txtLongURL"] );
	//Prevent known phishing sites to commit url
	if ( !empty( $URL ) && strLen( $URL ) > 4 && strPos( $URL,"paypal" ) === FALSE && strPos($URL,"16shop") === FALSE ){

		require_once $_SERVER["DOCUMENT_ROOT"].'/src/clsMyShortURL.class.php';
		$myShortURL = new clsMyShortURL();
		
		// Prevent injection into php/html
		$desc = filter_var( $_POST['txtDescription'], FILTER_SANITIZE_STRING );
		echo '<div class="container"><input class="asLabel" type="text" id="result" onclick="this.focus();this.select()" style="width:850px" value ="'.$myShortURL->fResultURL( $_POST['txtLongURL'], $desc ).'" readonly="readonly"></div>';

	}else{

		echo '<div class="container"><div class="alert alert-warning"><strong>It seems you have entered an empty or invalid url..</strong></div></div>';

	}//end if

}//end if



?>
